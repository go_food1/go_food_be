package go_food.controller.Pesanan;

import go_food.model.Pesanan.Pesanans;
import go_food.service.pesanans.PesananService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class pesananController {
    public static final Logger logger = LoggerFactory.getLogger(pesananController.class);

    @Autowired
    private PesananService pesananService;

    @PostMapping(value = "/pesanan")
    public ResponseEntity<?> pesanan(@RequestBody List<Pesanans> pesanans) throws Exception {
        pesananService.addPesanan(pesanans);
        logger.info("Success Pesan");
        return new ResponseEntity<>(pesanans, HttpStatus.CREATED);
    }
}
