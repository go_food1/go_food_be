package go_food.controller.category;

import go_food.model.categori.Category;
import go_food.service.category.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class CategoryController {
    public static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;

    //Add
    @PostMapping("/category")
    public ResponseEntity<?> create(@RequestBody Category category) throws Exception {
        logger.info("Creating category : {}", category);

        categoryService.add(category);

        return new ResponseEntity<>(category, HttpStatus.CREATED);
    }
    //FindAll
    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public ResponseEntity<List<Category>> get() {
        List<Category> dt = categoryService.getAll();

        return new ResponseEntity<>(dt, HttpStatus.OK);

    }
    //FindById
    @GetMapping("/category/{categoryId}")
    public ResponseEntity<?> findid(@PathVariable("categoryId") Integer id) throws Exception {
        Optional<Category> category = categoryService.findById(id);
        if (category.isPresent()) {
            logger.info("Fetching Category with id {}", id);
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
        logger.error("Category with id {} not found.", id);
        return new ResponseEntity<>("Category with id " + id + " not found", HttpStatus.NOT_FOUND);

    }
    @PutMapping("/category/{categoryId}")
    public ResponseEntity<?> edit(@PathVariable("categoryId") Integer id ,@RequestBody Category category) throws Exception {
        Category category1 = categoryService.update(id, category.getCategoryname());

        return new ResponseEntity<>(category1, HttpStatus.OK);
    }
    //DeleteById
    @DeleteMapping("/category/{categoryId}")
    public ResponseEntity<?> delete(@PathVariable("categoryId") Integer id) throws Exception {
        logger.info("Fetching & Deleting Category with id {}", id);

        categoryService.deleteById(id);
        return new ResponseEntity<>("Succes Deleted", HttpStatus.OK);
    }

}
