package go_food.controller.product;

import go_food.model.product.Product;
import go_food.service.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ProductController {
    public static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    //Add
    @PostMapping("/product")
    public ResponseEntity<?> create(@RequestBody Product product) throws Exception {
        logger.info("Creating product : {}", product);

        productService.add(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }
    //FindAll
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> get() {
        List<Product> dt = productService.getALl();

        return new ResponseEntity<>(dt, HttpStatus.OK);

    }
    //FindById
    @GetMapping("/product/{productId}")
    public ResponseEntity<?> findid(@PathVariable("productId") Integer id) throws Exception {
        Optional<Product> product = productService.findById(id);
        if (product.isPresent()) {
            logger.info("Fetching Product with id {}", id);
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        logger.error("Product with id {} not found.", id);
        return new ResponseEntity<>(("Product with id " + id + " not found"), HttpStatus.NOT_FOUND);

    }
    //EditedById
    @PutMapping("/product/{productId}")
    public ResponseEntity<?> edit(@PathVariable("productId") Integer id ,@RequestBody Product product) throws Exception {
        Product productt = productService.update(id, product.getName(), product.getPrice(), product.getImage());

        return new ResponseEntity<>(productt,HttpStatus.OK);
    }
    //DeleteById
    @DeleteMapping("/product/{productId}")
    public ResponseEntity<?> delete(@PathVariable("productId") Integer id) throws Exception {
        logger.info("Fetching & Deleting Product with id {}", id);

        productService.delete(id);
        return new ResponseEntity<>("Succes Deleted", HttpStatus.OK);
    }

}
