package go_food.model.Pesanan;

import com.fasterxml.jackson.annotation.JsonProperty;
import go_food.model.product.Product;

import javax.persistence.*;

@Entity
@Table(name = "pesanans")
public class Pesanans {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "keterangan")
    private String keterangan;

    @Column(name = "jumlah_Product")
    private Integer jumlahproduct;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "product_id")
    private Product product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getJumlahproduct() {
        return jumlahproduct;
    }

    public void setJumlahproduct(Integer jumlahproduct) {
        this.jumlahproduct = jumlahproduct;
    }

    public Product getProduct() {
        return product;
    }

    @JsonProperty("product")
    public void setProduct(Product product) {
        this.product = product;
    }
}
