package go_food.model.categori;

import com.fasterxml.jackson.annotation.JsonIgnore;
import go_food.model.product.Product;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "category")
    private String categoryname;

    @OneToMany(mappedBy = "category")
    private Set<Product> category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return categoryname;
    }

    public void setCategory(String category) {
        this.categoryname = category;
    }

    @JsonIgnore
    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public void setCategory(Set<Product> category) {
        this.category = category;
    }
}
