package go_food.model.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import go_food.model.Pesanan.Pesanans;
import go_food.model.categori.Category;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_product")
    private String name;

    @Column(name = "price")
    private Integer price;

    @Column(name = "image")
    private String image;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "category")
    private Category category;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    Set<Pesanans> pesanans;

    public Set<Pesanans> getPesanans() {
        return pesanans;
    }

    public void setPesanans(Set<Pesanans> pesanans) {
        this.pesanans = pesanans;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Category getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(Category category) {
        this.category = category;
    }
}
