package go_food.repository;

import go_food.model.Pesanan.Pesanans;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PesanansRepository extends JpaRepository<Pesanans, Integer> {
}
