package go_food.service.category;

import go_food.model.categori.Category;
import go_food.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    //Add
    public Category add(Category category) {
        Category category1 = new Category();
        category1.setCategoryname(category.getCategoryname());

        return categoryRepository.save(category1);
    }
    //GetAll
    public List<Category> getAll() {
        Category category = new Category();
        category.getCategoryname();
        return (List<Category>) categoryRepository.findAll();
    }
    //FindById
    public Optional<Category> findById(Integer id) {
        Category category = new Category();
        category.getCategoryname();
         return categoryRepository.findById(id);
    }
    //Edited
    public Category update(Integer id, String Categoryname) {
        Category category = categoryRepository.findById(id).orElse(null);
        category.setCategoryname(Categoryname);
        return categoryRepository.save(category);
    }
    //DeleteById
    public void deleteById(Integer id) {
        categoryRepository.deleteById(id);
    }
}
