package go_food.service.pesanans;

import go_food.model.Pesanan.Pesanans;
import go_food.model.product.Product;
import go_food.repository.PesanansRepository;
import go_food.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class PesananService {
    @Autowired
    private PesanansRepository pesanansRepository;
    @Autowired
    private ProductRepository productRepository;

    public List<Pesanans> addPesanan(List<Pesanans> pesanans){

        for (Pesanans pesan : pesanans){
            Product product = productRepository.findById(pesan.getProduct().getId()).orElse(null);
            pesan.setProduct(product);
            pesan.setKeterangan(pesan.getKeterangan());
            pesan.setJumlahproduct(pesan.getJumlahproduct());
        }
        return pesanansRepository.saveAll(pesanans);
    }
}
