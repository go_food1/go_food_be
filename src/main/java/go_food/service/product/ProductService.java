package go_food.service.product;

import go_food.model.categori.Category;
import go_food.model.product.Product;
import go_food.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    //Add
    public Product add(Product product) {
        Product product1 = new Product();
        product1.setName(product.getName());
        product1.setImage(product.getImage());
        product1.setCategory(product.getCategory());
        product1.setPrice(product.getPrice());

        return productRepository.save(product1);
    }
    //FindAll
    public List<Product> getALl () {
        Product product = new Product();
        product.getImage();
        product.getName();
        product.getPrice();
        product.getCategory();

        return (List<Product>) productRepository.findAll();
    }
    //FindById
    public Optional<Product> findById(Integer id) {
        Product product = new Product();
        product.getImage();
        product.getName();
        product.getPrice();
        product.getCategory();

        return productRepository.findById(id);
    }
    //Edited
    public Product update(Integer id, String Name, Integer Price, String Image) {
        Product product = productRepository.findById(id).orElse(null);

        product.setName(Name);
        product.setImage(Image);
        product.setPrice(Price);

        return productRepository.save(product);
    }
    //DeleteyId
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }
}
